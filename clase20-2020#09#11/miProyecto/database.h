#include <stdio.h>
#include <stdlib.h>

#define STR_NAME_LEN    30

#define DATABASE_WRITE_OK   1
#define DATABASE_WRITE_NOK  2

#define DATABASE_READ_NOK           1  
#define DATABASE_READ_FOUND         2
#define DATABASE_READ_NOT_FOUND     3


// modelos de datos => 4 + 30 + 4 + 4 + 4 = 46
struct persona {
    int id;
    char nombre[STR_NAME_LEN];
    int edad;
    float altura;
    float peso;
};

typedef struct persona PERSONA;


// prototipos CRUD
FILE* db_open ( char* file_name );
int db_close( FILE * file_name);
int write_record( FILE* fp, PERSONA* p);
int read_record(FILE* fp, int id, PERSONA* p);
void print_record( PERSONA* p);

void cargar( FILE* fp, PERSONA** database);
void listar(PERSONA** database);
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "database.h"

#define DATABASE_NAME   "db.dat"
#define DATABASE_SIZE   100

int main(){

    //PERSONA p = {1,"Juan", 30, 1.72, 70};
    PERSONA p = {2,"JPedro", 40, 1.5, 45};
    PERSONA q;

    PERSONA* mem_database[DATABASE_SIZE];

    FILE* database=NULL;
    int r;

    int id=0;
    int i=0;

    // abrir la base de datos
    database = db_open( DATABASE_NAME );
    if ( database == NULL){
        printf("main.c::main()::Error al abrir la base de datos\n");
        exit(-1);
    }

    //escribir en la base de datos
    // r = write_record( database, &p);
    // if (r != DATABASE_WRITE_OK){
    //     printf("main.c::main()::Error al escribir en la base de datos\n");
    //     //exit(-1);
    // }

    // id = 2;
    // r =  read_record(database, id, &q);
    // if (r == DATABASE_READ_FOUND){
    //     // imprimir el reegistro
    //     printf("main.c::main()::Registro con id = %d encontrado!\n", id);
    //     print_record(&q);
    // }
    // else {
    //     printf("main.c::main()::Registro con id = %d NO encontrado!\n", id);
    // }

    // inicializo el array
    for ( i=0; i<DATABASE_SIZE;i++)
        mem_database[i] =0;

    // cargamos la base de datos en memoria
    cargar( database, mem_database);

    // listar base datos
    listar( mem_database );


    // cerrar la base de datos
    r = db_close( database );
    if ( r == EOF){
        printf("main.c::main()::Error al cerra la base de datos\n");
        exit(-1);
    }

    return 0;
}
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
	pid_t child_pid;
	int child_status;

	printf("the main program process id is %d\n", (int)getpid());

	child_pid = fork();

	if (child_pid != 0)
	{
		printf("parent:: this is the parent process, me %d\n", (int)getpid());
		printf("parent:: this is the parent process, my child %d\n", (int)child_pid);

		/* Wait for the child process to complete. */
		wait(&child_status);

		if (WIFEXITED(child_status))
			printf("parent:: the child process exited normally, with exit code %d\n", WEXITSTATUS(child_status));
		else
			printf("parent:: the child process exited abnormally\n");

		return 0;
	}
	else
	{
		printf("child:: this is the child process, me %d\n", (int)getpid());
		printf("child:: this is the child process, my parent %d\n", (int)getppid());
		sleep(3);
	}
	return 0;
}

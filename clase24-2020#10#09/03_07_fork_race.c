#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(void)
{

    char c;
    pid_t pid;
    char filename[] = "data.text";

    int fd = open(filename, O_RDWR);
    if (fd == -1)
    {
        /* Handle error */
    }
    read(fd, &c, 1);
    printf("root process:%c\n", c);

    pid = fork();
    if (pid == -1)
    {
        /* Handle error */
    }

    if (pid == 0)
    { /*child*/
        read(fd, &c, 1);
        printf("child:%c\n", c);
    }
    else
    { /*parent*/
        sleep(1);
        read(fd, &c, 1);
        printf("parent:%c\n", c);
    }

    close(fd);
    return 0;
}
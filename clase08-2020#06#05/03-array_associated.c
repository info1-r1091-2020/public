/* 
 * Realizar un programa para la gestión de personas de riesgo, en donde se debe
 * ingresar el nombre y apellido en un solo campo y la edad.
 * Se desconoce la cantidad de personas, pero se sabe que no superará las 100.
 * El ingreso de datos finaliza con el nombre y apellido 'fin'.
 * Una vez finalizado el ingreso:
 *  - Emitir un listado con:
 *      cantidad de personas > 60 años
 *      cantidad de personas > que el promedio  
 */

#include <stdio.h>
#include <string.h>


// Cantidad máxima admisible de estudiantes
#define MAX          100

// Longitud maxima del string nombre y apellido
#define MAX_NYA      50

int  ingresar(char nya[][MAX_NYA], int* edad, int max);
void listar_mayores_sesenta(char nya[][MAX_NYA], int* edad, int c );
void listar_mayores_promedio(char nya[][MAX_NYA], int* edad, int c );

void vacia_buffer(void);
 
 int main(){

    char  nya[MAX][MAX_NYA];
    int   edad[MAX]; 

    // cantidad de datos ingresados
    int c;
    
    c = ingresar(nya, edad, MAX);
    listar_mayores_sesenta(nya, edad, c);
    listar_mayores_promedio(nya, edad, c);

    return 0;
 
 }

int  ingresar(char nya[][MAX_NYA], int* edad, int max){
    int i = 0;

    char aux_nya[MAX_NYA];
    int  aux_edad;


   while(1){
        printf("Ingrese nombre y apellido:");
        scanf("%s", aux_nya); 
        vacia_buffer();

        if (strcmp(aux_nya, "fin") == 0){
            break;
        }

        // desarrollar por el alumno
        printf("Ingrese la edad:");
        scanf("%d", &aux_edad);
        vacia_buffer();

        if ( i == max){
          printf("Se alcanzo el maximo de registros\n");
          break;
        }

        strcpy(nya[i], aux_nya);
        edad[i] = aux_edad;

        i++;

    }
    return i;
}

void listar_mayores_sesenta(char nya[][MAX_NYA], int* edad, int c ){
  int i=0;

  for (i=0;i<c;i++){
    if ( edad[i]>60)
    printf("%s\t %d\n", nya[i], edad[i]);
  }
}

void listar_mayores_promedio(char nya[][MAX_NYA], int* edad, int c ){

  // completar


}

void vacia_buffer(void){
  int ch;
  while ((ch = getchar()) != '\n' && ch != EOF);
}
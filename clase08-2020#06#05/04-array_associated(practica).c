/* 
 * Realizar un programa para la gestión de estudiantes, en donde se debe
 * ingresar el nombre, el apellido,  el legajo del estudiante y las notas de examenes.
 * Un estudiante puede tener varias notas (no todos los estudiante tiene la misma
 * cantidad de notas). Se desconoce la cantidad de estudiantes, pero se sabe que no
 * superará los 100 estudiantes.
 * El ingreso de datos finaliza con legajo cero (0). Se debe verificar que los valores
 * ingresados sean mayores a cero (0).
 * Una vez finalizado el ingreso, se debe informar:
 *  - Listar mejores promedio ‑ (estudiantes y nota) considerando estudiantes que hayan dado
 *    más examenes de dos (2) examenes.
 *  - Listar estudiantes que hayan dado dos (2) o menos examenes.
 *  - Listar todos los estudiantes con sus promedios.
 * 
 * Aclaracion: En ningun momento se solicita el detalle de cada nota individual
 *
 * version 0: solo legajo, cantidad de notas y promedio. Nota entre 0 y 10.
 * version 1: agregar nombre y apellido
 * version 2: comprobar legajo duplicado. Se pueden agregar notas en varias entradas diferentes.
 * version 3: comprobar buller lleno
 * version 4: mensaje si es necesario que no existe "mejor promedio"  
 */


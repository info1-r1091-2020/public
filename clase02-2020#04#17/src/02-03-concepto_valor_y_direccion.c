#include <stdio.h>

int main(){

int b = 99;     //definicion de una variable tipo entero
int* pb;     	//definicion de un puntero a int

pb = &b;

printf("\nEl valor de b=%d\t,la direccion(b) =%p, tamano(b) =%d bytes", b,&b,sizeof(b));
printf("\nEl valor de pb=%p\t,la direccion(pb)=%p, tamano(pb)=%d bytes\n", pb,&pb,sizeof(pb));

return 0;
}

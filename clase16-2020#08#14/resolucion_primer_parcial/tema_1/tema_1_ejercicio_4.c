#include <stdio.h>  // printf
#include <string.h> // memset, strlen, strcpy
#include <stdlib.h> // atoi

#define STR_MAX 100

#define OK       1
#define ERROR   -1


int remove_space(char *str);
int syntax_check(char *str);
int get_id(char *str, int *id);
int remove_char(char *data, char c);
int get_value ( char *data, float *value );

int main(void){

    //syntax ok
    char sensor_data_1[] = "{ 1, 5.2, true }";
    char sensor_data_2[] = "{ 21, 1.1, false}";
   
    int r=0,r1=0, r2=0;
    int id;
    float value;

    r1 = get_id(sensor_data_1, &id);
    r2 = get_value(sensor_data_1, &value);
    r = r1 & r2;
    if(r == OK) 
        printf("(id,value)=(%d,%.2f)\n", id,value);
    else
        printf("La sintaxis del string recibido es incorrecta\n");
  
    r1 = get_id(sensor_data_2, &id);
    r2 = get_value(sensor_data_2, &value);
    r = r1 & r2;
    if(r == OK) 
        printf("(id,value)=(%d,%.2f)\n", id,value);
    else
        printf("La sintaxis del string recibido es incorrecta\n");
  
    return 0;
}


int remove_space(char *data){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_space = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != 32 ){
            aux[j] = data[i];
            
            j++;
        }
        else {
            count_space++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de espacios eliminados
    return count_space;
}


int syntax_check(char *data){
    int l;
    int count_comma=0;
  
    // no lo pedia
    l = strlen(data);
    if ( l == 0 )
        return ERROR;

    // El string debe empezar con el carácter '{'
    //primer caracter '{'
    if (data[0] != '{')
        return ERROR;

    //ultimo caracter '}'   
    if (data[l-1] != '}')
        return ERROR;

    // encontrar dos comas ( es decir tres parametros)
    while( *data != 0){
        if ( *data == ',')
            count_comma++;
        data++;
    }
    if ( count_comma != 2)
        return ERROR;

    return OK;

}

int get_id(char *str, int *id){
    int r;
    char aux[STR_MAX];
    char *p;

    // verifico sintaxis y remuevo espacios
    r = syntax_check(str);
    if ( r == ERROR)
        return ERROR;

    // copia al string auxiliar
    strcpy(aux, str);

    // elimino la primera {
    remove_char(aux, '{');


    //reemplazo la primer ',' por un 0, asi obtengo el primer parametro
    p = aux;
    while( *p != ',')
        p++;
    *p = 0;
 
    // obtengo el id
    *id = atoi(aux);

    if ( *id >=0 && *id <= 100)
        return OK;
    
    return ERROR;


}

/*
 * remove char
 * return how many chars removed
 */
int remove_char(char *data, char c){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_char = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != c ){
            aux[j] = data[i];
            j++;
        }
        else {
            count_char++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de char eliminados
    return count_char;
}


int get_value ( char *str, float *value ){
    char aux[STR_MAX];
    int r=0;
    char *p;

    // verifico sintaxis y remuevo espacios
    r = syntax_check(str);
    if ( r == ERROR)
        return ERROR;

    // copia al string auxiliar
    strcpy(aux, str);

    // elimino la primera {
    remove_char(aux, '{');

    //me quedo con el string a partir de la primera ','
    p = aux;
    while( *p != ',')
        p++;
    p++;
    strcpy(aux,p); 
    
    //me quedo con el string a partir de la segunda ','
    p = aux;
    while( *p != ',')
        p++;
    p++;
    *p = 0;

    // obtengo el value
    *value = atof(aux);

    return OK;

}

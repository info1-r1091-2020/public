#include <stdio.h>  // printf
#include <string.h> // memset, strlen, strcpy
#include <stdlib.h> // atoi, atof

#define STR_MAX 100
#define ARR_SIZE 10
#define OK       1
#define ERROR   -1


int remove_space(char *str);
int syntax_check(char *str);
int get_id(char *str, int *id);
int remove_char(char *data, char c);
int get_value ( char *data, float *value );
void help(void);
void list_sensor(int *, float *, int idx_last);

int main( int argc, char **argv){

    int  arr_sensor_id[ARR_SIZE];
    float arr_sensor_value[ARR_SIZE];

    // aux
    char sensor_data[STR_MAX];
    int r=0,r1=0, r2=0;
    int id;
    float value;
    int i=0;
    int idx_last=0;

    if (argc > 1) {
        for(i=1;i<argc;i++){
            strcpy(sensor_data, argv[i]);
            r1 = get_id(sensor_data, &id);
            r2 = get_value(sensor_data, &value);
            r = r1 & r2;
            if ( r == OK ){
                arr_sensor_id[idx_last] = id;
                arr_sensor_value[idx_last] = value;
                idx_last++;
            }
        }
        list_sensor(arr_sensor_id, arr_sensor_value, idx_last);
    }
    else {
        help();
    }

 
    return 0;
}


int remove_space(char *data){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_space = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != 32 ){
            aux[j] = data[i];
            
            j++;
        }
        else {
            count_space++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de espacios eliminados
    return count_space;
}


int syntax_check(char *data){
    int l;
    int count_comma=0;
  
    // no lo pedia
    l = strlen(data);
    if ( l == 0 )
        return ERROR;

    // El string debe empezar con el carácter '{'
    //primer caracter '{'
    if (data[0] != '{')
        return ERROR;

    //ultimo caracter '}'   
    if (data[l-1] != '}')
        return ERROR;

    // encontrar dos comas ( es decir tres parametros)
    while( *data != 0){
        if ( *data == ',')
            count_comma++;
        data++;
    }
    if ( count_comma != 2)
        return ERROR;

    return OK;

}

int get_id(char *str, int *id){
    int r;
    char aux[STR_MAX];
    char *p;

    // verifico sintaxis y remuevo espacios
    r = syntax_check(str);
    if ( r == ERROR)
        return ERROR;

    // copia al string auxiliar
    strcpy(aux, str);

    // elimino la primera {
    remove_char(aux, '{');


    //reemplazo la primer ',' por un 0, asi obtengo el primer parametro
    p = aux;
    while( *p != ',')
        p++;
    *p = 0;
 
    // obtengo el id
    *id = atoi(aux);

    if ( *id >=0 && *id <= 100)
        return OK;
    
    return ERROR;


}

/*
 * remove char
 * return how many chars removed
 */
int remove_char(char *data, char c){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_char = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != c ){
            aux[j] = data[i];
            j++;
        }
        else {
            count_char++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de char eliminados
    return count_char;
}


int get_value ( char *str, float *value ){
    char aux[STR_MAX];
    int r=0;
    char *p;

    // verifico sintaxis y remuevo espacios
    r = syntax_check(str);
    if ( r == ERROR)
        return ERROR;

    // copia al string auxiliar
    strcpy(aux, str);

    // elimino la primera {
    remove_char(aux, '{');

    //me quedo con el string a partir de la primera ','
    p = aux;
    while( *p != ',')
        p++;
    p++;
    strcpy(aux,p); 
    
    //me quedo con el string a partir de la segunda ','
    p = aux;
    while( *p != ',')
        p++;
    p++;
    *p = 0;

    // obtengo el value
    *value = atof(aux);

    return OK;

}


void help(void){
    printf("Utilizar: myApp <data1> <data2>… <dataN>\n");
    printf("donde <dataN> es { <id>, <value>, <alarm state> }\n");
}

void list_sensor(int *id, float *value, int idx_last){
    int i;

    printf("\n\nListado:\n");
    for(i=0; i<idx_last;i++){
        printf("id=%d - value=%f\n", id[i], value[i]);
    }
}

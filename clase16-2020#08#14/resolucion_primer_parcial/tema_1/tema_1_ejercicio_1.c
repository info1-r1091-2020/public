#include <stdio.h>  // printf
#include <string.h> // memset, strlen, strcpy

#define STR_MAX 100

int remove_space(char *str);

int main(void){

    char str_test[STR_MAX] = " hola  mundo  ";
    int r;

    r = remove_space(str_test);

    printf("La cantidad de espacios eliminados fueron %d\n", r);
    printf("El string sin espacios es '%s'\n", str_test);

    return 0;
}


int remove_space(char *data){
    char aux[STR_MAX];
    int i = 0;
    int j = 0;
    int l;
    int count_space = 0;

    // inicializo el string auxiliar
    memset(aux, 0, STR_MAX);

    l = strlen(data);
    for(i=0; i<l;i++) {
        if ( data[i] != 32 ){
            aux[j] = data[i];
            
            j++;
        }
        else {
            count_space++;
        }
    }
    // el '\0' para que sea un string
    aux[j] = 0;
    
    // copio el string auxiliar al string original
    strcpy(data,aux);

    // revuelvo la cantidad de espacios eliminados
    return count_space;
}

# help
    git help

# configuracion
    git config --global user.name "Oscar Martin Paniagua"
    git config --global user.email opaniagua@frba.utn.edu.ar
    git config --global core.editor nano
    git config --global core.autocrlf input



Crea el archivo (texto plano) ~/.git-credentials puede ser un riesgo de seguridad

Lo utilizaremos mas adelante luego de crear el proyecto en GitLab


    git config credential.helper store
    
    git push [URL]
    
        Username: <type your username>
        Password: <type your password>


    git config -l

# crear un repositorio local

    mkdir -p /home/info/git

    cd /home/info/git

    mkdir test

    cd test

## Create an empty Git repository or reinitialize an existing one
    git init	

## view the status of your files in the working directory and staging area
    git status 	

## create example file 
    touch hello.c

## adds file contents to the staging area
    git status
    git add hello.c 	
    git status

## records a snapshot of the staging area
    git commit –m "Mi primer conmmit"
    git status	

## show commit logs
    git log	

## shows diff of what is staged and what is modified but unstaged
    git  diff 	

## reset current HEAD to the specified state
    git reset	


# crear un nuevo proyecto en GitLab
    https://gitlab.com/

## Sincronizar un directorio ya existente
    git init

El comando git remote añadirá un nuevo repositorio remoto, asociado al nombre "origin".
El nombre "origin" en Git, hace referencia al repositorio remoto de donde procede nuestro proyecto.

    git remote add origin [URL]

podemos comprobar si esa sincronización se ha realizado correctamente

    git remote show origin

ver todos los repositorios remotos que tenemos sincronizados.

    git remote -v

descargar la rama remota dentro del repositorio local.

    git pull origin master

sincronizar la rama master local con la remota (origin)

    git fetch --set-upstream origin master


Subiendo los cambios al repositorio remoto
    
    ;git push origin [rama]
    git push origin master


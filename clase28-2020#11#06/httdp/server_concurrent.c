#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <sys/stat.h>
#include <fcntl.h>


#define PORT		8100
#define MAX_CONN	10

#define BUFFER_SIZE	256


void handle_connection (int connection_fd);
void handle_get (int connection_fd, char* page);


int main(void){

    int local_port = PORT;
    int max_connections = MAX_CONN;
    int	aux; 
	pid_t child_pid;


    // -- variables auxiliares para el manejo del system call de socket --
    int	server_socket;
    struct sockaddr_in my_addr;
    int newfd; 												/* Por este socket duplicado del inicial se transaccionará*/
    struct sockaddr_in their_addr;  						/* Contendra la direccion IP y número de puerto del cliente */
	unsigned int sin_size = sizeof(struct sockaddr_in);

    // -- 1) crear un socket --
	if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
		fprintf(stderr, "Error en función socket. Código de error %s\n", strerror(server_socket));
		return -1;
	}

    // eliminar timeout
    int reuse  = 1;
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse , sizeof(reuse)); 

    // -- 2) asignamos valores a la estructura my_addr --
    
    // -- familia de sockets INET para UNIX  --
	my_addr.sin_family = AF_INET;		
	
    // -- convierte el entero formato PC a entero formato network --
    my_addr.sin_port = htons(local_port);	
	
    // -- automaticamente usa la IP local (todas las que tenga) --
    my_addr.sin_addr.s_addr = INADDR_ANY;	
	
    // -- rellena con ceros el resto de la estructura  --
    bzero(&(my_addr.sin_zero), 8);



    // -- 3) con la estructura sockaddr_in completa, se declara en el Sistema que este 
    //       proceso escuchará pedidos por la IP y el port definidos --
	if ( (aux = bind (server_socket, (struct sockaddr *) &my_addr, sizeof(struct sockaddr))) == -1)
	{
		fprintf(stderr, "Error en función bind. Código de error %s\n", strerror(aux));
		return -1;
	}
	
    // -- 4) Habilitamos el socket para recibir conexiones, con una cola de conexiones 
    //       en espera que tendrá como máximo el tamaño especificado en max_connections --
	//       listen es 'non bloking'
    if ((aux = listen (server_socket, max_connections)) == -1)
	{
		fprintf(stderr, "Error en función listen. Código de error %s\n", strerror(aux));
		return -1;
    }


   while(1){

	    // -- 5) se espera por conexiones, accept es 'bloking'  
        // -- Extrae el primer pedido de conexion de la cola de conexiones
        // -- pendientes del socket s, le asocia un nuevo socket que es un
        // -- duplicado de s y le reserva un nuevo file descriptor --

	    if ((newfd = accept(server_socket, (struct sockaddr *)&their_addr, &sin_size)) == -1)
	    {
		    fprintf(stderr, "Error en función accept. Código de error %s\n", strerror(newfd));
		    return -1;
	    }
	    else
	    {
		    printf  ("server:  conexión desde:  %s\n", inet_ntoa(their_addr.sin_addr));
            
			

			child_pid = fork ();
			if (child_pid == 0) {

				// proceso hijo

				// cierro los descriptores que no voy a necesitar
				close (STDIN_FILENO);
				close (STDOUT_FILENO);
				close (server_socket);

				handle_connection (newfd);

				close (newfd);
				
				exit (0);
				
			}
			else if (child_pid > 0) {
				// proceso padre
				
				// no la necesita el proceso padre
				close (newfd);

			}
			else {
				
				fprintf(stderr, "fork error");
			}

			
			
			
	    }
   }


    return 0;
}




void handle_connection (int connection_fd){
	char buffer[BUFFER_SIZE];
	int bytes_read;
	char method[BUFFER_SIZE];
	char url[BUFFER_SIZE];
	char protocol[BUFFER_SIZE];
    char response[1024];


	// 400
	char* bad_request_response = 
	  "HTTP/1.0 400 Bad Request\n"
	  "Content-type: text/html\n"
	  "\n"
	  "<html>\n"
	  " <body>\n"
	  "  <h1>Bad Request</h1>\n"
	  "  <p>This server did not understand your request.</p>\n"
	  " </body>\n"
	  "</html>\n";

	// 501
	char* bad_method_response_template = 
	  "HTTP/1.0 501 Method Not Implemented\n"
	  "Content-type: text/html\n"
	  "\n"
	  "<html>\n"
	  " <body>\n"
	  "  <h1>Method Not Implemented</h1>\n"
	  "  <p>The method %s is not implemented by this server.</p>\n"
	  " </body>\n"
	  "</html>\n";


  // -- read from client
  bytes_read = read (connection_fd, buffer, sizeof (buffer) - 1);

  if (bytes_read > 0) {

    buffer[bytes_read] = '\0';
  
	// -- debug
	fprintf(stderr, "-----\n");
	fprintf(stderr, "%s", buffer);
	fprintf(stderr, "\n-----\n");
  
	// -- la primera linea recibida es el HTTP request
    sscanf (buffer, "%s %s %s", method, url, protocol);
   
	// los headers enviados por el cliente, los leo y en la respuesta lo envio
    while (strstr (buffer, "\r\n\r\n") == NULL){
      bytes_read = read (connection_fd, buffer, sizeof (buffer));
    }
	
    if (bytes_read == -1) {
      close (connection_fd);
      return;
    }
   
   if (strcmp (protocol, "HTTP/1.0") && strcmp (protocol, "HTTP/1.1")) {
      write (connection_fd, bad_request_response, sizeof (bad_request_response));
   }
   else if (strcmp (method, "GET")) {

      snprintf (response, sizeof (response), bad_method_response_template, method);
      write (connection_fd, response, strlen (response));
    }
   else 
      handle_get (connection_fd, url);
  }
  else if (bytes_read == 0)
	// el cliente cerro la conexion
	;
  else 
    // error
    fprintf(stderr, "read error");
}




void handle_get (int connection_fd, char* page){

		
	char response[] = 
		"HTTP/1.1 200 OK\r\n"
		"Content-Type: text/html; charset=UTF-8\r\n\r\n"
		"<!DOCTYPE html><html><head><title>info-utn</title></head>"
		"<body>"
		"<h5>Hello r1091!</h5>"
		"</body>"
		"</html>\r\n";

	write(connection_fd, response, sizeof(response) - 1); 
	

}




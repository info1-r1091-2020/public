/*
 * daemonize.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#define RUNNING_DIR "/tmp"

void create_daemon(void);

int main()
{
    create_daemon();

    while (1)
    {
        //TODO: aqui va el codigo de nuestro programa

        // -- enviar al syslog informacion de 'start-up'
        syslog(LOG_NOTICE, "daemon started.");
        sleep(20);
        break;
    }

    // -- enviar al syslog informacion de 'end'
    syslog(LOG_NOTICE, "daemon terminated.");

    // -- cerrar el syslog
    closelog();

    return EXIT_SUCCESS;
}

void create_daemon(void)
{
    pid_t pid;
    pid_t sid = 0;

    // -- primer fork
    pid = fork();

    if (pid < 0)
        exit(EXIT_FAILURE);

    // -- terminar el proceso padre
    if (pid > 0)
        exit(EXIT_SUCCESS);

    // -- crear una nueva sesion(del proceso hijo) y se desasocia del TTY
    // -- Cada proceso es miembro de un grupo y estos a su vez se reúnen en sesiones
    // -- En cada una de estas hay un proceso que hace las veces de líder, de tal forma que
    // -- si muere todos los procesos de la sesión reciben una señal SIGHUP. La idea es que
    // -- el líder muere cuando se quiere dar la sesión por terminada, por lo que mediante
    // -- SIGHUP se notifica al resto de procesos esta circunstancia para que puedan terminar
    // -- ordenadamente. Obviamente no estamos interesados en que el demonio termine cuando la
    // -- sesión desde la que fue creado finalice, por lo que necesitamos crear nuestra propia
    // -- sesión de la que dicho demonio será el líder
    sid = setsid();
    if (sid < 0)
    {
        // Return failure
        exit(EXIT_FAILURE);
    }

    // -- TODO: implementar los handlers de señales que nos interese
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);

    // -- segundo fork, para asegurar que se desasocio del TTY
    pid = fork();

    if (pid < 0)
        exit(EXIT_FAILURE);

    // -- terminar el proceso padre
    if (pid > 0)
        exit(EXIT_SUCCESS);

    // -- set de los permisos
    // -- La máscara —denominada umask()— es heredada de padres a hijos por los procesos,
    // -- por lo que su valor por defecto será el mismo que el que tenía configurada la shell
    // -- que lanzó el demonio. Esto significa que el demonio no sabe que permisos acabarán
    // -- teniendo los archivos que intente crear. Para evitarlo simplemente podemos autorizar
    // -- todos los permisos o podriamos establecer el que necesitemos:
    umask(0);

    // -- cambiar el directorio de trabajo
    chdir(RUNNING_DIR);

    // -- cerrar todos los files descriptors abiertos automaticamente
    // -- por el proceso padre
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // -- abrir el syslog (servicio de log o eventos)
    openlog("test_daemon", LOG_PID, LOG_DAEMON);
}

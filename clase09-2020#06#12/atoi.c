#include<stdio.h>
#include<string.h>

int atoi (char*);

//"1565" == 5 + 60 + 500 + 1000
//pow
int main (void){

    int a;
    
    a= atoi ("12345");
    printf ("%d\n",a);

    return 0;

}

int atoi (char *numero){
    int resultado = 0;
    int largo;
    int i;
    int multiplicador;
    int digito;
    largo = strlen (numero);
    
    for (i=0;i<largo;i++)
    {
        if(i>0)
            multiplicador = multiplicador * 10;
        else
            multiplicador = 1;
        
        digito = numero[largo-1-i] - '0';
        resultado = resultado + digito*multiplicador;
    }
    
    return resultado;
    
}

# Clase #3

## Tipo de Datos

- Se realizaron ejercicios con 3 de los 4 tipos de datos de C
	- int
	- char
	- float
	- double (pendiente)
	
* Se repasaron los conceptos de declaracion e inicializacion de variables	
* Se utilizo printf, con %2.f (placeholders) para variables tipo float

Preguntas:

- Para una variable tipo float, para la parte decimal se utiliza '.' o ','? Porque? 
- Que sucede si a una variable tipo int le asigno el valor 15.5?
	La función printf que nos muestra?
- Porque utilizamos %d para imprimir una variable tipo char?
- Que sucede si a una variable tipo char le asigno el numero 129?
	La función printf que nos muestra en este caso?

## Practica "Calculadora"

Con este ejercicio aplicamos los siguientes temas aprendidos:

- Utilización de los operadores aritmetricos: + - * /
- Utilización de las estructuras if-else
- Utilización de #define para valores constantes 
- En la funcion division, tuvimos la necesidad de realizar una validacion, la division por cero. 
- Repaso de funciones con parametros por valor y referencia (punteros)


## Practica "triangulo"

Con este ejercicio aplicamos los siguientes temas aprendidos:

- Concepto de algotirmo 
- Utilización de las estructuras if anidado (nested)

## Practica "swap"

Con este ejercicio aplicamos los siguientes temas aprendidos:

- Repaso de punteros
- Necesidad de utilizacion de variables auxiliares en nuestro algotimo de swap 


## Practica de "punteros"

Con este ejercicio aplicamos los siguientes temas aprendidos de punteros:

- Declarar un puntero
- Inicializar un puntero
- Utilización del Operador indirección '*' (valor apuntado por...)
		
Preguntas:

- Cual es el tamaño de un int? y el de un int *?
- Cual es el tamaño de un char? y el de un char *?
- Porque el tamaño de int * y char * son iguales?
- Porque es necesario para una variable tipo puntero indicarle el tipo (int, char, etc...)?
- Cuando se declara un puntero...como buena practica, con que valor inicializarlo?




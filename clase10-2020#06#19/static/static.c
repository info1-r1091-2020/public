#include <stdio.h>

void f(void);

int main() {
	f();
	f();
	f();
	
    return 0;
}

void f(void) {
	static int x = 0;   // x se inicializa solo una vez
	printf("%d\n", x);
	x = x + 1;
}

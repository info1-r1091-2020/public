#include <stdio.h>
#include <stdlib.h>

#define FILE_CSV "MOCK_DATA.csv"
#define FILE_LINE_LEN 200

int main(void)
{

    FILE *file;
    char str[FILE_LINE_LEN];

    // abro el archivo
    file = fopen(FILE_CSV, "r");
    if (file == NULL)
    {
        printf("Arhivo csv no encontrado\n");
        exit(-1);
    }

    // proceso el archivo
    // fgets lee de a lineas, hasta encontrar 'newline' o
    // un fin de archivo 'EOF' o el tamaño maximo especificado
    fgets(str, FILE_LINE_LEN, file);

    // imprimo en pantalla
    fputs(str, stdout);

    while (fgets(str, FILE_LINE_LEN, file) != NULL)
    {
        fputs(str, stdout);
    }

    // agrego un registro ( una linea de texto ) al final del archivo
    if (fseek(file, 0L, SEEK_END) != 0)
    {
        // TODO: manejar condicion de error
        printf("Error fseek\n");
        exit(-1);
    }

    // cierro el archivo
    fclose(file);

    return 0;
}
#include <stdio.h>
#include <stdlib.h>

#define FILE_CSV "MOCK_DATA.csv"
#define FILE_LINE_LEN 200

int main(void)
{

    FILE *file;
    char str[FILE_LINE_LEN];

    // abro el archivo
    file = fopen(FILE_CSV, "r+");
    if (file == NULL)
    {
        printf("Arhivo csv no encontrado\n");
        exit(-1);
    }

    // agrego un registro ( una linea de texto ) al final del archivo
    if (fseek(file, 0L, SEEK_END) != 0)
    {
        // TODO: manejar condicion de error
        printf("Error fseek\n");
        exit(-1);
    }
    fputs("1001,Oscar,46,1.72,71\n", file);

    // cierro el archivo
    fclose(file);

    return 0;
}
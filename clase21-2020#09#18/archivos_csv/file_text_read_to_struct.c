#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_CSV "MOCK_DATA.csv"
#define FILE_LINE_LEN 200

#define STR_NAME_LEN 30

struct persona
{
    int id;
    char nombre[STR_NAME_LEN];
    int edad;
    float altura;
    float peso;
};

typedef struct persona PERSONA;

void remove_char(char *s, int c);
void string_to_struct(char *str_persona, PERSONA *p);

int main(void)
{

    FILE *file;
    char str[FILE_LINE_LEN];

    PERSONA p;

    // abro el archivo
    file = fopen(FILE_CSV, "r");
    if (file == NULL)
    {
        printf("Arhivo csv no encontrado\n");
        exit(-1);
    }

    // proceso el archivo
    // fgets lee de a lineas, hasta encontrar 'newline' o
    // un fin de archivo 'EOF' o el tamaño maximo especificado

    while (fgets(str, FILE_LINE_LEN, file) != NULL)
    {
        string_to_struct(str, &p);
        printf("nombre: %s\n", p.nombre);
    }

    // cierro el archivo
    fclose(file);

    return 0;
}

void string_to_struct(char *str_persona, PERSONA *p)
{
    const char s[2] = ",";
    char *token;

    // elimino los espacios
    remove_char(str_persona, ' ');

    // obtengo el primer token
    token = strtok(str_persona, s);
    p->id = atoi(token);

    // obtengo los siguientes token's
    token = strtok(NULL, s);
    strcpy(p->nombre, token);

    token = strtok(NULL, s);
    p->edad = atoi(token);

    token = strtok(NULL, s);
    p->altura = atof(token);

    token = strtok(NULL, s);
    p->peso = atof(token);
}

void remove_char(char *s, int c)
{
    int n = strlen(s);

    int i = 0;
    int j = 0;

    for (; i < n; i++)
        if (s[i] != c)
            s[j++] = s[i];

    s[j] = '\0';
}
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "database.h"

#define VERSION "0.0.2"

void menu(FILE **, PERSONA **, int *);
void header_menu(int count);
void blue(void);
void red(void);
void yellow(void);
void reset(void);
void option_menu(int number, char *str);
void option_exit_menu(int number, char *str);

void no_implementada(void);

int main()
{

    PERSONA *mem_database[DATABASE_SIZE];
    FILE *file_database = NULL;
    int count_database = 0;

    menu(&file_database, mem_database, &count_database);

    return 0;
}

void menu(FILE **file, PERSONA **database, int *count)
{
    PERSONA p;
    int op = -1;
    int id = 0;

    // abro el archivo de  base de datos
    db_open(file, DATABASE_NAME);

    // cargo el array
    *count = cargar(*file, database);

    while (op != 0)
    {
        //system("cls");
        header_menu(*count);
        printf("\n");
        printf("Informatica I r1091 -- Menu Principal: \n\n");
        option_menu(1, "Buscar por id");
        option_menu(2, "Buscar por nombre");
        option_menu(3, "Listar");
        option_menu(4, "Insertar un registro");
        option_menu(5, "Eliminar un registro por id");
        option_menu(6, "Importar desde arhivo csv");
        option_menu(7, "Exportar a un archivo csv");
        printf("\n");
        option_exit_menu(0, "Salir/Terminar");
        printf("\n");
        printf("Indica la opcion: ");
        scanf("%d", &op);

        switch (op)
        {
        case 1:
            //buscar por id
            printf("ingrese id:");
            scanf("%d", &id);
            buscar_por_id(database, id);
            break;
        case 2:
            //buscar por nombre
            no_implementada();
            break;
        case 3:
            //listar
            listar(database);
            break;
        case 4:
            //insertar
            printf("Ingrese persona, formato: id nombre edad altura peso\n");
            scanf("%d %s %d %f %f", &p.id, p.nombre, &p.edad, &p.altura, &p.peso);
            insertar(*file, database, count, &p);
            printf("main.c::menu(): Registros cargados %d\n", *count);
            break;
        case 5:
            //eliminar
            no_implementada();
            break;
        case 6:
            //bulk
            //https://www.mockaroo.com
            insertar_bulk(*file, database, count);
            printf("main.c::menu(): Registros cargados %d\n", *count);
            break;
        case 7:
            //export
            no_implementada();
            break;
        }
    }

    // libero memoria
    liberar(database);

    // cierro la base de datos
    db_close(*file);
}

void no_implementada(void)
{
    printf("main.c::menu(): Funcion no implementada\n");
    return;
}

void header_menu(int count)
{

    printf("**************************************************************\n");
    printf("**** ");
    blue();
    printf("Mi Proyecto v%s ", VERSION);
    reset();
    printf("**************************************\n");
    printf("****                                                      ****\n");
    printf("****                          Cantidad de Registros: %d **** \n", count);
    printf("****                                                      ****\n");
    printf("**************************************************************\n");
}

void blue(void)
{
    printf("\033[1;34m");
}

void red(void)
{
    printf("\033[1;31m");
}

void yellow(void)
{
    printf("\033[1;33m");
}

void reset(void)
{
    printf("\033[0m");
}

void option_menu(int number, char *str)
{
    yellow();
    printf("[%d]  ", number);
    reset();
    printf("%s\n", str);
}

void option_exit_menu(int number, char *str)
{
    red();
    printf("[%d]  ", number);
    reset();
    printf("%s\n", str);
}
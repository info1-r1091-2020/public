# Changelog

## [0.0.3] - 2020-09-25

### Added ( pending )

- Add buscar_por_nombre()
- Add eliminar_por_id()
- Add export()
- Add log

## [0.0.2] - 2020-09-18

### Added

- Start versioning [@Oscar Paniagua](https://github.com/opaniagu).
- Add menu [@Oscar Paniagua](https://github.com/opaniagu).
- Add init_mem_database()
- Add buscar_por_id()
- Add insertar()
- Add insertar_bulk()
- Add string_to_struct()

### Changed

- Fixed memory free [@Oscar Paniagua](https://github.com/opaniagu).
- Rewrite db_open() [@Oscar Paniagua](https://github.com/opaniagu).
- Fixed db_close() to check if file pointer is NULL [@Oscar Paniagua](https://github.com/opaniagu).
- Modified Makefile add target compile

#include "database.h"

int db_open(FILE **fp, char *file_name)
{
    *fp = fopen(file_name, "a+");
    return DATABASE_OPEN_OK;
}

int db_close(FILE *fp)
{
    if (fp != NULL)
        return fclose(fp);
    return 0;
}

int write_record(FILE *fp, PERSONA *p)
{
    //TODO: verificaciones ( archivo, PERSONA)

    // me posiciono al final del archivo
    if (fseek(fp, 0, SEEK_END) != 0)
    {
        //TODO:
        return DATABASE_WRITE_NOK;
    }

    if (fwrite(p, sizeof(PERSONA), 1, fp) == 0)
    {
        //TODO:
        return DATABASE_WRITE_NOK;
    }
    return DATABASE_WRITE_OK;
}

// funcion que lee un registro
// busca por el campo id
// suponemos que el id es unico y que los registros no estan ordenados
int read_record(FILE *fp, int id, PERSONA *p)
{
    PERSONA aux;
    int r;

    //TODO: realizar las verificaciones que consideren

    // me posiciono al principio
    if (fseek(fp, 0, SEEK_SET) != 0)
    {
        //TODO:
        return DATABASE_READ_NOK;
    }

    while ((r = fread(&aux, sizeof(PERSONA), 1, fp)) != 0)
    {
        if (aux.id == id)
        {
            //copio la estructura a 'p'
            *p = aux;
            return DATABASE_READ_FOUND;
        }
    }
    return DATABASE_READ_NOT_FOUND;
}

void print_record(PERSONA *p)
{
    if (p != NULL)
    {
        printf("----------------\n");
        printf("id: %d\n", p->id);
        printf("nombre: %s\n", p->nombre);
        printf("edad: %d\n", p->edad);
        printf("altura: %f\n", p->altura);
        printf("peso: %f\n", p->peso);
        printf("----------------\n");
    }
    else
    {
        printf("database.c::print_record():: Registro recibido NULL\n");
    }
}

int cargar(FILE *fp, PERSONA **database)
{
    PERSONA aux;
    int count = 1;
    //TODO: verificar

    liberar(database);

    init_mem_database(database);

    if (fseek(fp, 0, SEEK_SET) != 0)
    {
        //TODO
        exit(-1);
    }

    while ((fread(&aux, sizeof(PERSONA), 1, fp)) != 0)
    {
        *database = (PERSONA *)malloc(sizeof(PERSONA));
        //TODO: comprobar malloc

        //copio estructura
        **database = aux;
        database++;
        count++;
    }

    return count;
}

void listar(PERSONA **database)
{
    //TODO: verificar

    while (*database != 0)
    {
        print_record(*database);
        database++;
    }
}

void liberar(PERSONA **database)
{
    // TODO: verificar tamaño maximo del array
    while (*database != 0)
    {
        free(*database);
        database++;
    }
}

void buscar_por_id(PERSONA **database, int id)
{
    // TODO: verificar tamaño maximo del array
    while (*database != 0)
    {
        if ((*database)->id == id)
        {
            print_record(*database);
            return;
        }
        database++;
    }
    printf("database::cargar:: Registro con id %d no encontrado\n", id);
}

void init_mem_database(PERSONA **database)
{
    int i;
    // inicializo el array
    for (i = 0; i < DATABASE_SIZE; i++)
        database[i] = 0;
}

void insertar(FILE *fp, PERSONA **database, int *count, PERSONA *p)
{

    //busco un elemento del array vacio para insertar el registro
    int i = 0;

    while (*database != 0 && i < DATABASE_SIZE)
    {
        database++;
        i++;
    }

    if (i >= DATABASE_SIZE)
    {
        printf("database::insertar:: Capacidad maxima alcanzada\n");
        return;
    }

    *database = (PERSONA *)malloc(sizeof(PERSONA));
    //TODO: comprobar malloc

    //copio estructura
    **database = *p;

    //guardo en el archivo
    write_record(fp, p);

    *count = *count + 1;
}

void eliminar_por_id(PERSONA **database, int id)
{
}

void insertar_bulk(FILE *fp, PERSONA **database, int *count)
{
    FILE *fp_csv;
    char str[FILE_LINE_LEN];
    PERSONA p;
    int i = 0;

    // abro el archivo csv
    fp_csv = fopen(FILE_CSV, "r+");
    if (fp_csv == NULL)
    {
        printf("Arhivo csv no encontrado\n");
        return;
    }

    // proceso el archivo
    // fgets lee de a lineas, hasta encontrar 'newline' o un fin de archivo 'EOF' o el tamaño maximo especificado
    fgets(str, FILE_LINE_LEN, fp_csv);

    while (fgets(str, FILE_LINE_LEN, fp_csv) != NULL && i < DATABASE_SIZE)
    {
        string_to_struct(str, &p);
        //write_record(fp, &p);
        insertar(fp, database, count, &p);
        i++;

        if (*count >= DATABASE_SIZE)
        {
            printf("Se alcanzo el maximo de tamaño de la base de datos\n");
            break;
        }
    }

    // cierro el archivo
    fclose(fp_csv);
}

// error en atof
// https://stackoverflow.com/questions/7708309/atof-not-working/7708355
//
void string_to_struct(char *str_persona, PERSONA *p)
{
    const char s[2] = ",";
    char *token;

    // TODO: realizar verificaciones del string, ejemplo: cantidad de parametros, tipos, etc

    // espero recibir: "{ 1, "Juan", 12, 1.40, 40 }"

    //
    //printf("debug::string_to_struct:: string %s\n", str_persona);

    // elimino los espacios
    remove_char(str_persona, ' ');

    // elimino las llaves de inicio y fin
    remove_char(str_persona, '{');
    remove_char(str_persona, '}');

    //
    //printf("debug::string_to_struct:: string %s\n", str_persona);

    // obtengo el primer token
    token = strtok(str_persona, s);
    p->id = atoi(token);

    // obtengo los siguientes token's
    token = strtok(NULL, s);
    strcpy(p->nombre, token);

    token = strtok(NULL, s);
    p->edad = atoi(token);

    token = strtok(NULL, s);
    p->altura = atof(token);

    token = strtok(NULL, s);
    p->peso = atof(token);
}

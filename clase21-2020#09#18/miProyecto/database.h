#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helped.h"

#define DATABASE_NAME "db.dat"
#define DATABASE_SIZE 25000

#define STR_NAME_LEN 30

#define DATABASE_OPEN_OK 1
#define DATABASE_OPEN_NOK 2

#define DATABASE_WRITE_OK 1
#define DATABASE_WRITE_NOK 2

#define DATABASE_READ_NOK 1
#define DATABASE_READ_FOUND 2
#define DATABASE_READ_NOT_FOUND 3

#define FILE_CSV "MOCK_DATA.csv"
#define FILE_LINE_LEN 200

// modelos de datos => 4 + 30 + 4 + 4 + 4 = 46
struct persona
{
    int id;
    char nombre[STR_NAME_LEN];
    int edad;
    float altura;
    float peso;
};

typedef struct persona PERSONA;

// prototipos CRUD
//FILE *db_open(char *file_name);

int db_open(FILE **fp, char *file_name);

int db_close(FILE *file_name);
int write_record(FILE *fp, PERSONA *p);
int read_record(FILE *fp, int id, PERSONA *p);
void print_record(PERSONA *p);

int cargar(FILE *fp, PERSONA **database);
void listar(PERSONA **database);

// clase 19/09/20
void liberar(PERSONA **database);
void buscar_por_id(PERSONA **database, int id);
void insertar(FILE *fp, PERSONA **database, int *count, PERSONA *p);
void eliminar_por_id(PERSONA **database, int id);
void init_mem_database(PERSONA **database);
void insertar_bulk(FILE *fp, PERSONA **database, int *count);
void string_to_struct(char *str_persona, PERSONA *p);
